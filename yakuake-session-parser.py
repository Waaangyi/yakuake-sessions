#!/usr/bin/env python
import argparse
import sys
import subprocess
import os
import signal

def get_stdout(command):
    return subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").strip()

def get_num(name):
    session_list = get_stdout(["qdbus", "org.kde.yakuake", "/yakuake/sessions", "org.kde.yakuake.sessionIdList"]).split(",")
    for i in session_list:
        if(get_stdout(["qdbus", "org.kde.yakuake", "/yakuake/tabs", "org.kde.yakuake.tabTitle", i]) == name):
            return int(i) + 1
    return -1

def run_command_in_tab(name, command, raise_window):
    id = get_num(name)
    if(id == -1):
        print("Bad name")
        return 1
    # Before doing anything, kill the running process
    processId = get_stdout(["qdbus", "org.kde.yakuake", "/Sessions/" + str(id), "processId"])
    foregroundProcessId = get_stdout(["qdbus", "org.kde.yakuake", "/Sessions/" + str(id), "foregroundProcessId"])
    # Don't do anything if the shell hasn't been created yet
    if(int(processId) > 1):
        # Kill the running process
        if(processId != foregroundProcessId):
            os.kill(int(foregroundProcessId), signal.SIGKILL)
        # Send INT to the shell process
        os.kill(int(processId), signal.SIGINT)


    if(raise_window):
        get_stdout(["qdbus", "org.kde.yakuake", "/yakuake/sessions", "raiseSession", str(id - 1)])
    get_stdout(["qdbus", "org.kde.yakuake", "/Sessions/" + str(id), "runCommand", command])

def dbus(command):
    subprocess.run("qdbus org.kde.yakuake " + command, shell = True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run command in yakuake with tab names instead of session IDs")
    parser.add_argument("-n", "--name", dest="name", help="Name of the tab", metavar="NAME", required = True)
    parser.add_argument('-r', action="store_true", dest="raise_window")
    #parser.add_argument("-r", "--raise_window", dest="raise_window", help="raises the session")
    parser.add_argument("-c", "--command", dest="command", help="command", metavar='COMMAND')
    parser.add_argument("-d", "--dbus", dest="dbus", help="DBUS command", metavar='COMMAND')
    arg = parser.parse_args()

    if(arg.command):
        sys.exit(run_command_in_tab(arg.name, arg.command, arg.raise_window)
)
    if(arg.dbus):
        sys.exit(dbus(arg.dbus))
